#include <WiFi.h>
#include <HTTPClient.h>

const char* ssid = "myIOTwifi";
const char* password = "correcthorsebatterystable";
const String pushoverToken = "supersecret123";
const String pushoverUser = "supersecret1234";

// from left to right
const String PlantNames[12] = {
  "aaa",
  "bbb",
  "ccc",
  "ddd",
  "eee",
  "fff",
  "ggg",
  "hhh",
  "iii",
  "jjj",
  "kkk",
  "lll"
};
const int PlantPins[12]   = {32,   26,   12,   34,   33,   25,   27,   14,   13,   0,    2,    15};
const int AirValues[12]   = {2900, 2900, 2900, 2900, 2900, 2900, 2900, 2900, 2900, 2900, 2900, 2900};
const int WaterValues[12] = {1300, 1300, 1300, 1300, 1300, 1300, 1300, 1300, 1300, 1300, 1300, 1300};


// root ca from pushover
const char* root_ca= \
"-----BEGIN CERTIFICATE-----\n" \
"MIIDrzCCApegAwIBAgIQCDvgVpBCRrGhdWrJWZHHSjANBgkqhkiG9w0BAQUFADBh\n" \
"MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3\n" \
"d3cuZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBD\n" \
"QTAeFw0wNjExMTAwMDAwMDBaFw0zMTExMTAwMDAwMDBaMGExCzAJBgNVBAYTAlVT\n" \
"MRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5j\n" \
"b20xIDAeBgNVBAMTF0RpZ2lDZXJ0IEdsb2JhbCBSb290IENBMIIBIjANBgkqhkiG\n" \
"9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4jvhEXLeqKTTo1eqUKKPC3eQyaKl7hLOllsB\n" \
"CSDMAZOnTjC3U/dDxGkAV53ijSLdhwZAAIEJzs4bg7/fzTtxRuLWZscFs3YnFo97\n" \
"nh6Vfe63SKMI2tavegw5BmV/Sl0fvBf4q77uKNd0f3p4mVmFaG5cIzJLv07A6Fpt\n" \
"43C/dxC//AH2hdmoRBBYMql1GNXRor5H4idq9Joz+EkIYIvUX7Q6hL+hqkpMfT7P\n" \
"T19sdl6gSzeRntwi5m3OFBqOasv+zbMUZBfHWymeMr/y7vrTC0LUq7dBMtoM1O/4\n" \
"gdW7jVg/tRvoSSiicNoxBN33shbyTApOB6jtSj1etX+jkMOvJwIDAQABo2MwYTAO\n" \
"BgNVHQ8BAf8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUA95QNVbR\n" \
"TLtm8KPiGxvDl7I90VUwHwYDVR0jBBgwFoAUA95QNVbRTLtm8KPiGxvDl7I90VUw\n" \
"DQYJKoZIhvcNAQEFBQADggEBAMucN6pIExIK+t1EnE9SsPTfrgT1eXkIoyQY/Esr\n" \
"hMAtudXH/vTBH1jLuG2cenTnmCmrEbXjcKChzUyImZOMkXDiqw8cvpOp/2PV5Adg\n" \
"06O/nVsJ8dWO41P0jmP6P6fbtGbfYmbW0W5BjfIttep3Sp+dWOIrWcBAI+0tKIJF\n" \
"PnlUkiaY4IBIqDfv8NZ5YBberOgOzW6sRBc4L0na4UU+Krk2U886UAb3LujEV0ls\n" \
"YSEY1QSteDwsOoBrp+uvFRTp2InBuThs4pFsiv9kuXclVzDAGySj4dzp30d8tbQk\n" \
"CAUw7C29C79Fv1C5qfPrmAESrciIxpg0X40KPMbp1ZWVbd4=\n" \
"-----END CERTIFICATE-----\n";

const int LedPin = 5;
int soilMoistureValue = 0;
int soilmoisturepercent = 0;
 

void connectToNetwork() {
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to iot wifi..");
  }
  Serial.println("Connected!");
}


void setup() {
  Serial.begin(115200); // open serial port, set the baud rate to 9600 bps
  pinMode(LedPin, OUTPUT);

  connectToNetwork();
  Serial.println(WiFi.localIP());
  Serial.println(WiFi.macAddress());
}


void loop() {
  for (int i = 0; i < 12; i++) {
    digitalWrite(LedPin, HIGH);

    soilMoistureValue = analogRead(PlantPins[i]);  // put Sensor insert into soil
    Serial.print(PlantNames[i]);
    Serial.print(": ");
    soilmoisturepercent = map(soilMoistureValue, AirValues[i], WaterValues[i], 0, 100);

    if(soilmoisturepercent > 100) {
      Serial.print("100 %");
    } else if(soilmoisturepercent < 0) {
      Serial.print("0 %");
    } else if(soilmoisturepercent >= 0 && soilmoisturepercent <= 100) {
      Serial.print(soilmoisturepercent);
      Serial.print(" %");
    }
    Serial.print(" (");
    Serial.print(soilMoistureValue);
    Serial.println(")");

    if(soilmoisturepercent < 50) {
      if ((WiFi.status() == WL_CONNECTED)) {
        HTTPClient http;
        http.begin("https://api.pushover.net/1/messages.json", root_ca);
        http.addHeader("Content-Type", "application/x-www-form-urlencoded");
        String httpRequestData = "token=" + pushoverToken + "&user=" + pushoverUser + "&message=Moisture%20level%20of%20" + PlantNames[i] + "%20dropped%20to%20" + soilmoisturepercent + "%25";
        int httpResponseCode = http.POST(httpRequestData);
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        http.end();
      }
    }

    digitalWrite(LedPin, LOW);
    delay(100);
  }
  Serial.println("sleeping for 10mins..");
  delay(600000);  // 10mins
}
