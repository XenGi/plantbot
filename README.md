# plantbot

Small shitty Arduino plant bot measuring moisture for up to 12 plants using ESP32.

# BOM

- 12x Capacitive Soil Moisture Sensor v1.2
- 1x [Wemos Lolin D32][d32] or compatible ESP32 devboard
- 1x Breadboard (18x24)
- 2x Male pin headers 1x16
- 2x Female socket headers 1x16
- 12x Male pin headers 1x3
- Cables

# Build

- Solder Male pin headers 1x16 to the devboard
- Solder Female socket headers 1x16 to the breadboard
- Solder Male pin headers 1x3 to the breadboard
- Connect all GND of sensor headers together
- Connect all VCC of sensor headers together
- Connect GND, VCC and DATA lines to devboard headers

![breadboard][breadboard]

# Configure

- adapt the folowing values in the code:
  - ssid: your wifi name
  - password: your wifi password
  - pushoverToken: token for pushover service
  - pushoverUser: user token for pushover service
  - PlantNames: names of your plants
  - AirValues: readings of sensors when in air
  - WaterValues: readings of sensors when in water
- flash the code with arduino IDE
  - if flashing fails try without the sensors attached

---

[d32]: https://www.wemos.cc/en/latest/d32/d32.html
[breadboard]: https://gitlab.com/XenGi/plantbot/-/raw/main/breadboard.png
